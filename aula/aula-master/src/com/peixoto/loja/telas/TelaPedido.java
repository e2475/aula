package com.peixoto.loja.telas;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.pedido.ServicePedido;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.repositorio.PedidoRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class TelaPedido implements Tela {
    String clienteAReceberPedido;
    final private Scanner entradaEstoque;
    final private ClienteRepositorio clienteRepositorio;
    final private ProdutoRepositorio produtoRepositorio;
    final private PedidoRepositorio pedidoRepositorio;

    public TelaPedido(Scanner entradaEstoque, ProdutoRepositorio produtoRepositorio, ClienteRepositorio clienteRepositorio, PedidoRepositorio pedidoRepositorio) {
        this.entradaEstoque = entradaEstoque;
        this.produtoRepositorio = produtoRepositorio;
        this.clienteRepositorio = clienteRepositorio;
        this.pedidoRepositorio = pedidoRepositorio;
    }


    public void abrir() {

        System.out.println("Bem vindo ao sistema de pedidos, amigo");
        int opcao;
        do {
            try {
                System.out.println("Informe o CPF do cliente:");
                this.clienteAReceberPedido = this.entradaEstoque.nextLine();
                boolean clienteEncontradoNaBusca = this.clienteRepositorio.clienteExiste(clienteAReceberPedido);
                if (clienteEncontradoNaBusca) {
                    System.out.println("Bem vindo, Sr(a) " + this.clienteRepositorio.buscarClientePorCpf(clienteAReceberPedido).getNome());
                    System.out.println("1 - Realizar um Pedido");
                    System.out.println("2 - Listar pedidos");
                    System.out.println("3 - Pesquisar Pedidos por Data");
                    System.out.println("4 - Pesquisar Pedidos por Codigo");
                    opcao = Integer.parseInt(this.entradaEstoque.nextLine());
                    if (opcao == 1) {
                        ServicePedido servicePedido = new ServicePedido(entradaEstoque, produtoRepositorio, pedidoRepositorio);
                        servicePedido.gravaDadosPedido(clienteRepositorio.buscarClientePorCpf(clienteAReceberPedido));


                    }
                    if (opcao == 2) {
                        ServicePedido servicePedido = new ServicePedido(entradaEstoque, produtoRepositorio, pedidoRepositorio);
                        servicePedido.listarPedidos();

                    }
                    if (opcao == 3) {
                        ServicePedido servicePedido = new ServicePedido(entradaEstoque, produtoRepositorio, pedidoRepositorio);
                        System.out.println("Digite a data a pesquisar em formato dd/MM/yyyy");
                        String data = (this.entradaEstoque.nextLine());
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                        LocalDate date = LocalDate.parse(data, formatter);
                        servicePedido.buscarPedidoPorData(date);

                    }
                    if (opcao == 4) {
                        ServicePedido servicePedido = new ServicePedido(entradaEstoque, produtoRepositorio, pedidoRepositorio);
                        System.out.println("Digite o codigo do pedido que deseja buscar:");
                        Integer codigoAPesquisar = Integer.parseInt(entradaEstoque.nextLine());
                        servicePedido.buscarPedidoPorCodigo(codigoAPesquisar);

                    }

                }
                if (!clienteEncontradoNaBusca) {
                    throw new Exception("O CPF está incorreto ou o cliente não está cadastrado");

                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("1 - Inserir outro CPF");
                System.out.println("2 - Sair do Sistema de Pedidos");
                opcao = Integer.parseInt(this.entradaEstoque.nextLine());
                if (opcao == 2) {

                    break;
                }

            }


        } while (!clienteRepositorio.clienteExiste(clienteAReceberPedido));


    }


}

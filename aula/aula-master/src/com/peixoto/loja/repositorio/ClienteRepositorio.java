package com.peixoto.loja.repositorio;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.telas.TelaCliente;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class ClienteRepositorio implements Repositorio<Cliente> {


    private static final List<Cliente> listaClientes = new ArrayList<>();

    public ClienteRepositorio() {
        if(listaClientes.isEmpty()) {
            listaClientes.add(new Cliente("111", "Lauro O Freitas", "R: Wilson"));
            listaClientes.add(new Cliente("222", "Milton", "R: Pedro"));
            listaClientes.add(new Cliente("333", "Caroline", "R: Afonso"));
        }
    }

    public void salvar(Cliente cliente) {


        listaClientes.add(cliente);
    }

    public List<Cliente> buscarTodos() {
        return listaClientes;
    }

    public Cliente buscarClientePorCpf(String cpf) {
        Cliente clienteEncontrado = null;
        for (Cliente clienteNaLista : listaClientes) {
            if (clienteNaLista.getCpf().equals(cpf)) {
                clienteEncontrado = clienteNaLista;
            }
        }
        return clienteEncontrado;
    }

    public boolean clienteExiste(String cpf) {
        for (Cliente clienteNaLista : listaClientes) {
            if (clienteNaLista.getCpf().equals(cpf)) {
                return true;
            }
        }
        return false;
    }

    public List<Cliente> buscarClienteporNome(String nome) {
        List<Cliente> listaClientesPesquisa = new ArrayList<>();
        for(Cliente clienteNaLista : listaClientes) {
            if(clienteNaLista.getNome().trim().toLowerCase().contains(nome.trim().toLowerCase())) {
                listaClientesPesquisa.add(clienteNaLista);
            }
        }
        return listaClientesPesquisa;
    }

}

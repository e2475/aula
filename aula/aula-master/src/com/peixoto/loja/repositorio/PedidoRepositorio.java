package com.peixoto.loja.repositorio;
import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.pedido.Pedido;
import java.util.ArrayList;
import java.util.List;

public class PedidoRepositorio implements Repositorio<Pedido>{

    public static final List<Pedido> listaPedido = new ArrayList<>();


    public void salvar(Pedido pedido) {
        listaPedido.add(pedido);
    }

    public List<Pedido> buscarTodos() {
        return listaPedido;
    }


}

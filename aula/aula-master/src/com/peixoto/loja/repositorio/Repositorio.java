package com.peixoto.loja.repositorio;

import com.peixoto.loja.estoque.Produto;

import java.util.List;

public interface Repositorio<T>{

    void salvar(T object);

    List<T> buscarTodos();


}

package com.peixoto.loja.cadastro;

import com.peixoto.loja.pedido.Pedido;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Cliente {
    private String cpf;
    private String nome;
    private String endereco;
    private String status;

    public Cliente(String cpf, String nome, String endereco) {
        this.cpf = cpf;
        this.nome = nome;
        this.endereco = endereco;
    }
    public Cliente(){
        super();
    }

    public  ArrayList<Pedido> getListaPedidoCliente() {
        return listaPedidoCliente;
    }

    public void setListaPedidoCliente(ArrayList<Pedido> listaPedidoCliente) {
        Cliente.listaPedidoCliente = listaPedidoCliente;
    }

    public static ArrayList<Pedido> listaPedidoCliente = new ArrayList<>();

    public LocalDate getDataFimVigencia() {
        return dataFimVigencia;
    }

    public void setDataFimVigencia(LocalDate dataFimVigencia) {
        this.dataFimVigencia = dataFimVigencia;
    }

    private LocalDate dataFimVigencia;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}

package com.peixoto.loja.pedido;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.repositorio.PedidoRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.peixoto.loja.repositorio.PedidoRepositorio.listaPedido;

public class ServicePedido {
    int quantidadeItem;
    long codigoItem;
    double valorCarrinho;
    final private Scanner entradaEstoque;
    final private ProdutoRepositorio produtoRepositorio;
    final private PedidoRepositorio pedidoRepositorio;

    static int codPedido = 0;

    public ServicePedido(Scanner entradaEstoque, ProdutoRepositorio produtoRepositorio, PedidoRepositorio pedidoRepositorio) {
        this.entradaEstoque = entradaEstoque;
        this.produtoRepositorio = produtoRepositorio;
        this.pedidoRepositorio = pedidoRepositorio;
    }


    public void gravaDadosPedido(Cliente clienteAreceberPedido) throws Exception {
        int opcao;
        Pedido pedido = new Pedido();
        do {
        System.out.println("Aperte 1 se deseja visualizar os produtos disponíveis:");
        System.out.println("Aperte 0 para continuar");
        opcao = Integer.parseInt(this.entradaEstoque.nextLine());
        if (opcao != 0) {
            mostrarProdutosDisponiveis();
        }

            System.out.println("Digite o codigo do item:");
            this.codigoItem = Long.parseLong(this.entradaEstoque.nextLine());

            System.out.println("Digite a quantidade:");
            this.quantidadeItem = Integer.parseInt(this.entradaEstoque.nextLine());
            addItem(pedido, codigoItem);

            valorCarrinho = valorCarrinho + this.produtoRepositorio.buscarProdutoPorCodigo(codigoItem).getValor() * quantidadeItem;
            System.out.println("0 - Finalizar Pedido");
            System.out.println("1 - Continuar adicionando itens");
            System.out.println("2 - Listar itens no carrinho");
            opcao = Integer.parseInt(this.entradaEstoque.nextLine());
            if(opcao == 2){
                opcao = 0;
                mostrarItensPedido(pedido);

                System.out.println("1 - alterar item");
                System.out.println("2 - finalizar Pedido");
                opcao = Integer.parseInt(this.entradaEstoque.nextLine());
                if(opcao == 1){
                        double quantidadeAntiga = this.quantidadeItem;
                    System.out.println("Digite o codigo do item a ser alterado");
                    Long itemAserAlterado = Long.parseLong(this.entradaEstoque.nextLine());
                    System.out.println("Digite a  nova quantidade:");
                    this.quantidadeItem = Integer.parseInt(this.entradaEstoque.nextLine());
                    if (this.quantidadeItem == 0){
                        List<ItemPedido> listaItemPedido = pedido.getListaItemPedido();
                        for (ItemPedido itemPedido: listaItemPedido){
                            listaItemPedido.remove(buscaritemPorCodigo(itemAserAlterado, pedido));
                            itemPedido.getProduto().setEstoque(itemPedido.getProduto().getEstoque()+quantidadeAntiga);





                        }
                    }


                }
            }


            pedido.setPreco(valorCarrinho);

        }while (opcao == 1);

        LocalDate date = LocalDate.now();
        pedido.setDataPedido(date);
        pedido.setCliente(clienteAreceberPedido);
        pedido.setCodigo(geraCodPedido());

        pedidoRepositorio.salvar(pedido);
        System.out.println("Pedido salvo com sucesso!!");


    }

    private void mostrarProdutosDisponiveis() throws Exception {
        ProdutoRepositorio produtoRepositorio = new ProdutoRepositorio();
        List<Produto> produtos = produtoRepositorio.buscarTodos();
        try {
            if (produtos.isEmpty()) {
                throw new Exception("Não há nenhum produto disponivel");
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        for (Produto produto : produtos) {

            if (produto.getEstoque() > 0) {
                System.out.println("Código: " + produto.getCodigo() + "|" + "Nome: " + produto.getNome() + " | Valor: " + produto.getValor() + " | Quantidade estoque: " + produto.getEstoque());
            }

        }

    }

    public void addItem(Pedido pedido, long codigoItem) {
        ItemPedido itemPedido = new ItemPedido();

        itemPedido.setProduto(produtoRepositorio.buscarProdutoPorCodigo(codigoItem));
        itemPedido.setQuantidade(this.quantidadeItem);
        itemPedido.setPreco(produtoRepositorio.buscarProdutoPorCodigo(codigoItem).getValor());
        List<ItemPedido> listaItemPedido = pedido.getListaItemPedido();
        if (listaItemPedido == null) {

            pedido.setListaItemPedido(new ArrayList<>());
        }
        itemPedido.getProduto().setEstoque(itemPedido.getProduto().getEstoque() - itemPedido.getQuantidade());

        pedido.getListaItemPedido().add(itemPedido);


    }

    public int geraCodPedido() {


        codPedido = codPedido + 1;

        return codPedido;

    }

    public void mostrarPedidos(List<Pedido> pedidos) {
        for (Pedido pedido : pedidos) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            System.out.println("Código: " + pedido.getCodigo() + "|" + "Nome Cliente: " + pedido.getCliente().getNome() + " | Data: " + pedido.getDataPedido().format(formatter) + " | Valor R$: " + pedido.getPreco() + "\n");
            mostrarItensPedido(pedido);
        }
    }

    private void mostrarItensPedido(Pedido pedido) {
        List<ItemPedido> listaItemPedido = pedido.getListaItemPedido();
        for(ItemPedido itemPedido : listaItemPedido){
            System.out.println("Codigo: " + itemPedido.getProduto().getCodigo() + " | Nome: "+ itemPedido.getProduto().getNome() + " | Quantidade: " +itemPedido.getQuantidade() + " | Valor R$: " + itemPedido.getPreco());
        }
    }

    public void listarPedidos() {

        try {
            List<Pedido> pedidos = this.pedidoRepositorio.buscarTodos();
            mostrarPedidos(pedidos);
            if (pedidos.isEmpty()) {
                throw new Exception("Não há nenhum pedido para este cliente");
            }
            System.out.println("-----------------------------------------------------------");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("-----------------------------------------------------------");
        }
    }

    public void buscarPedidoPorData(LocalDate data) {
        Pedido pedidoEncontrado = null;
        List<Pedido> listaPedido = pedidoRepositorio.buscarTodos();

        for (Pedido pedidoNaLista : listaPedido) {
            if (pedidoNaLista.getDataPedido().equals(data)) {
                pedidoEncontrado = pedidoNaLista;
            }
        }
        assert pedidoEncontrado != null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Código: " + pedidoEncontrado.getCodigo() + " | " + "Nome Cliente: " + pedidoEncontrado.getCliente().getNome() + " | Data: " + pedidoEncontrado.getDataPedido().format(formatter) + " | Valor R$: " + pedidoEncontrado.getPreco() + "\n");


    }

    public void buscarPedidoPorCodigo(Integer codigo) {
        Pedido pedidoEncontrado = null;
        List<Pedido> listaPedido = pedidoRepositorio.buscarTodos();
        for (Pedido pedidoNaLista : listaPedido) {
            if (pedidoNaLista.getCodigo().equals(codigo)) {
                pedidoEncontrado = pedidoNaLista;
            }
        }
        assert pedidoEncontrado != null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Código: " + pedidoEncontrado.getCodigo() + " | " + "Nome Cliente: " + pedidoEncontrado.getCliente().getNome() + " | Data: " + pedidoEncontrado.getDataPedido().format(formatter) + " | Valor R$: " + pedidoEncontrado.getPreco() + "\n");
    }
    public ItemPedido buscaritemPorCodigo(Long codigo, Pedido pedido) {
        ItemPedido itemEncontrado = null;
        List<ItemPedido> listaItemPedido = pedido.getListaItemPedido();
        for (ItemPedido itemNaLista : listaItemPedido) {
            if (itemNaLista.getProduto().getCodigo().equals(codigo)) {
                itemEncontrado = itemNaLista;

            }
        }
        return itemEncontrado;

    }


}

package com.peixoto.loja.pedido;

import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.repositorio.PedidoRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;
import com.peixoto.loja.repositorio.Repositorio;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ServicePedido {
    int quantidadeItem;
    long codigoItem;
    final private Scanner entradaEstoque;
    final private ProdutoRepositorio produtoRepositorio;

    public ServicePedido(Scanner entradaEstoque, ProdutoRepositorio produtoRepositorio) {
        this.entradaEstoque = entradaEstoque;
        this.produtoRepositorio = produtoRepositorio;
    }


    public void gravaDadosPedido(ProdutoRepositorio produtoRepositorio) throws Exception {
        int opcao = 0;
        Pedido pedido = new Pedido();
        System.out.println("Aperte 1 se deseja visualizar os produtos disponíveis:");
        System.out.println("Aperte 0 para continuar");
        opcao = Integer.parseInt(this.entradaEstoque.nextLine());
        if (opcao != 0) {
            mostrarProdutosDisponiveis();
        }
        System.out.println("Digite o codigo do item:");
        this.codigoItem= Long.parseLong(this.entradaEstoque.nextLine());

        PedidoRepositorio pedidoRepositorio = new PedidoRepositorio();
        pedidoRepositorio.salvar(pedido);

        System.out.println("Digite a quantidade:");
        this.quantidadeItem = Integer.parseInt(this.entradaEstoque.nextLine());
        addItem(pedido , codigoItem );
    }

    private void mostrarProdutosDisponiveis() throws Exception {
        ProdutoRepositorio produtoRepositorio = new ProdutoRepositorio();
        List<Produto> produtos = produtoRepositorio.buscarTodos();
        try {
            if (produtos.isEmpty()) {
                throw new Exception("Não há nenhum produto disponivel");
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        for (Produto produto : produtos) {

            if (produto.getEstoque() > 0) {
                System.out.println("Código: " + produto.getCodigo() + "|" + "Nome: " + produto.getNome() + " | Valor: " + produto.getValor() + " | Quantidade estoque: " + produto.getEstoque());
            }

        }

    }

public void addItem(Pedido pedido, long codigoItem){
    ItemPedido itemPedido = new ItemPedido();

itemPedido.setProduto(produtoRepositorio.buscarProdutoPorCodigo(codigoItem));
itemPedido.setQuantidade(this.quantidadeItem);
itemPedido.setPreco(produtoRepositorio.buscarProdutoPorCodigo(codigoItem).getValor());
    List<ItemPedido> listaItemPedido = pedido.getListaItemPedido();
if(listaItemPedido==null){

    pedido.setListaItemPedido(new ArrayList<>());
}
itemPedido.getProduto().setEstoque(itemPedido.getProduto().getEstoque() - itemPedido.getQuantidade());

    pedido.getListaItemPedido().add(itemPedido);


}






}

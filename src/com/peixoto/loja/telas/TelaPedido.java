package com.peixoto.loja.telas;
import com.peixoto.loja.estoque.Produto;
import com.peixoto.loja.pedido.ServicePedido;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;

import java.util.Scanner;

public class TelaPedido implements Tela {
    String clienteAReceberPedido;
    final private Scanner entradaEstoque;
    final private ClienteRepositorio clienteRepositorio;
    final private ProdutoRepositorio produtoRepositorio;

    public TelaPedido(Scanner entradaEstoque, ProdutoRepositorio produtoRepositorio, ClienteRepositorio clienteRepositorio) {
        this.entradaEstoque = entradaEstoque;
        this.produtoRepositorio = produtoRepositorio;
        this.clienteRepositorio = clienteRepositorio;
    }


    public void abrir() {

        System.out.println("Bem vindo ao sistema de pedidos, amigo");
        int opcao;
        do {
            try {
                System.out.println("Informe o CPF do cliente:");
                this.clienteAReceberPedido = this.entradaEstoque.nextLine();
                boolean clienteDoEncontradoNaBusca = this.clienteRepositorio.clienteExiste(clienteAReceberPedido);
                if (clienteDoEncontradoNaBusca) {
                    System.out.println("1 - Realizar um Pedido");
                    System.out.println("2 - Listar pedidos");
                    opcao = Integer.parseInt(this.entradaEstoque.nextLine());
                    if (opcao == 1) {
                        ServicePedido servicePedido = new ServicePedido(entradaEstoque, produtoRepositorio);
                        servicePedido.gravaDadosPedido(produtoRepositorio);



                    }
                    if (opcao == 2) {


                    }

                }
                if (!clienteDoEncontradoNaBusca) {
                    throw new Exception("O CPF está incorreto ou o cliente não está cadastrado");

                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("1 - Inserir outro CPF");
                System.out.println("2 - Sair do Sistema de Pedidos");
                opcao = Integer.parseInt(this.entradaEstoque.nextLine());
                if (opcao == 2) {

                    break;
                }

            }


        } while (!clienteRepositorio.clienteExiste(clienteAReceberPedido));


    }



}

package com.peixoto.loja.telas;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;

import java.util.Scanner;

public class TelaPrincipal implements Tela {




    public void abrir() {
        int opcao;
        Scanner entrada = new Scanner(System.in);
        System.out.println("***************Bem Vindo Amigo Administrador da Loja****************");
        do {
            System.out.println("0 - Fechar Sistema");
            System.out.println("1 - Abrir Tela do Estoque");
            System.out.println("2 - Abrir Tela de Clientes");
            System.out.println("3 - Abrir Tela de Pedidos");
            opcao = Integer.parseInt(entrada.nextLine());
            if(opcao == 1) {
                new TelaEstoque(entrada).abrir();
            }
            if(opcao == 2) {
                new TelaCliente(entrada).abrir();
            }
            if(opcao == 3) {
                ProdutoRepositorio produtoRepositorio = new ProdutoRepositorio();
                ClienteRepositorio clienteRepositorio = new ClienteRepositorio();
                new TelaPedido(entrada, produtoRepositorio, clienteRepositorio).abrir();
            }

        }while (opcao != 0);
        System.out.println("Fechou a Tela");

    }

}

package com.peixoto.loja.telas;

import com.peixoto.loja.cadastro.Cliente;
import com.peixoto.loja.repositorio.ClienteRepositorio;


import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class TelaCliente implements Tela {

    final private Scanner entradaEstoque;
    final private ClienteRepositorio clienteRepositorio;

    public TelaCliente(Scanner entradaEstoque) {
        this.entradaEstoque = entradaEstoque;
        this.clienteRepositorio = new ClienteRepositorio();
    }

    public void abrir() {

        System.out.println("Bem vindo ao sistema de gerenciamento de clientes, amigo");
        int opcao;
        do {
            System.out.println("0 - Sair do sistema de Clientes");
            System.out.println("1 - Cadastrar Cliente");
            System.out.println("2 - Listar Clientes");
            System.out.println("3 - Buscar cliente por CPF");
            System.out.println("4 - Buscar cliente por Nome");
            opcao = Integer.parseInt(this.entradaEstoque.nextLine());
            if (opcao == 1) {
                Cliente cliente = entrarDadosCliente();
                this.clienteRepositorio.salvar(cliente);
                System.out.println("Cliente cadastrado com sucesso!!");
            }
            if (opcao == 2) {
                try {
                    List<Cliente> clientes = this.clienteRepositorio.buscarTodos();
                    mostrarClientes(clientes);
                    if (clientes.isEmpty()) {
                        throw new Exception("Não há nenhum cliente cadastrado");
                    }
                    System.out.println("-----------------------------------------------------------");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    System.out.println("-----------------------------------------------------------");
                }
            }

            if (opcao == 3) {
                try {
                    System.out.println("Informe o CPF do cliente: ");
                    String cpfASerPesquisado = this.entradaEstoque.nextLine();
                    Cliente clienteDoEncontradoNaBusca = this.clienteRepositorio.buscarClientePorCpf(cpfASerPesquisado);
                    if (clienteDoEncontradoNaBusca == null) {
                        throw new Exception("O CPF está incorreto ou o cliente não está cadastrado");
                    }
                    System.out.println("O Clinete é: ");
                    System.out.println("Nome: " + clienteDoEncontradoNaBusca.getNome() + "\nCPF: " + clienteDoEncontradoNaBusca.getCpf() + " \nEndereço:" + clienteDoEncontradoNaBusca.getEndereco());
                    System.out.println("------------------------------------------------------------------------------------------");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            if (opcao == 4) {


                System.out.println("Digite o nome do cliente par buscar:");
                String nomeASerPesquisado = this.entradaEstoque.nextLine();
                List<Cliente> clienteDoEncontradoNaBusca = this.clienteRepositorio.buscarClienteporNome(nomeASerPesquisado);
                System.out.println("Cliente:");
                for (Cliente clinteNaLista : clienteDoEncontradoNaBusca) {
                    System.out.println(clinteNaLista.getNome());
                }


            }

        } while (opcao != 0);
    }

    private void mostrarClientes(List<Cliente> clientes) {

        for (Cliente cliente : clientes) {
            System.out.println("Nome: " + cliente.getNome() + "|" + "Status: " + cliente.getStatus() + " | CPF: " + cliente.getCpf() + " | Endereço: " + cliente.getEndereco());
        }

    }

    private Cliente entrarDadosCliente() {
        Cliente cliente = new Cliente();
        System.out.println("Informe o Nome: ");
        cliente.setNome(this.entradaEstoque.nextLine());
        System.out.println("Informe o CPF: ");
        cliente.setCpf(this.entradaEstoque.nextLine());
        System.out.println("Informe o Endereço: ");
        cliente.setEndereco(this.entradaEstoque.nextLine());
        cliente.setDataFimVigencia(calculaDataVigencia(cliente));
        defineStatusCliente(cliente);

        return cliente;
    }

    public int calculaTamanhoNome(Cliente cliente) {

        return cliente.getNome().length();
    }

    public LocalDate calculaDataVigencia(Cliente cliente) {

        return LocalDate.now().minusDays(10).plusDays(calculaTamanhoNome(cliente));
    }

    public void defineStatusCliente(Cliente cliente) {
        LocalDate date = LocalDate.now();
        if (date.isAfter(calculaDataVigencia(cliente))) {
            cliente.setStatus("Inativo");
        } else {
            cliente.setStatus("Ativo");
        }

    }


}

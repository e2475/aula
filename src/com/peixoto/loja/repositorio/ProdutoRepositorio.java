package com.peixoto.loja.repositorio;

import com.peixoto.loja.estoque.Produto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoRepositorio implements Repositorio<Produto> {

    public static final List<Produto> listaProdutos = new ArrayList<>();

    public void salvar(Produto produto) {
        listaProdutos.add(produto);
    }

    public List<Produto> buscarTodos() {
        return listaProdutos;
    }

    public Produto buscarProdutoPorCodigo(Long codigo) {
        Produto produtoDoEncontrado = null;
        for (Produto produtoNaLista : listaProdutos) {
            if (produtoNaLista.getCodigo().equals(codigo)) {
                produtoDoEncontrado = produtoNaLista;
            }
        }
        return produtoDoEncontrado;
    }

}
